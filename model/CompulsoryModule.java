package courseman2.model;


/**
 * @overview Compulsory is a sub-class of Module 
 */

public class CompulsoryModule extends Module
{
    public CompulsoryModule(String n, int s, int r)
    {
        super(n, s, r);
    }
    
}
